# AWS API configuration
node.default[:aws][:ec2_access_key] = ENV['EC2_ACCESS_KEY']
node.default[:aws][:ec2_secret] = ENV['EC2_SECRET']

# S3 API configuration
node.default[:aws][:s3_access_key] = ENV['S3_ACCESS_KEY']
node.default[:aws][:s3_secret] = ENV['S3_SECRET']
node.default[:aws][:s3_bucket] = ENV['S3_BUCKET']

# Get regions to perform the benchmark in (provided via cloud-init.sh script)
# Example: export AWS_REGIONS="eu-west-1,sa-east-1,ap-southeast-2,us-east-1"
node.default[:aws][:regions] = ENV['AWS_REGIONS']

# Get images to perform the benchmark with (provided via cloud-init.sh script)
# Example: export AWS_IMAGES="ami-1f8c8e6b,ami-06875f1b,ami0bb92e31,ami-e50e888c"
# Images must have the same order as their corresponding regions
node.default[:aws][:images] = ENV['AWS_IMAGES']

# Get instance types to perform benchmark on (provided via cloud-init.sh script)
# Example: export AWS_INSTANCE_TYPES="t1.micro,m1.small,m1.xlarge,m2.2xlarge,c1.medium,c1.xlarge"
node.default[:aws][:itypes] = ENV['AWS_INSTANCE_TYPES']

# Get hours when to perform benchmark on instances (provided via cloud-init.sh script)
# Example: export BENCH_HOURS="0,3,6,9,12,15,18,21"
node.default[:hours] = ENV['BENCH_HOURS']

# Logfile
node.default[:logfile] = "/root/benchmark_trigger.log"