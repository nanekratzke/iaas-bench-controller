#! /bin/bash

# Provide EC2 API credentials
export EC2_ACCESS_KEY="<EC2 access key>"
export EC2_SECRET="<EC secret access key>"

# Provide S3 credentials and bucket for uploading benchmark results
export S3_ACCESS_KEY="<your access key>"
export S3_SECRET="<your secret access key>"
export S3_BUCKET="<bucket to store benchmark results>"

# Provide benchmark setup
# Instance types to benchmark (use API identifiers)
export AWS_INSTANCE_TYPES="t1.micro,m1.small,m1.xlarge,m2.2xlarge,c1.medium,c1.xlarge"
# Regions to benchmark (use Region identifiers)
export AWS_REGIONS="eu-west-1,sa-east-1,ap-southeast-2,us-east-1"
# Images to use for benchmark (must have the same order as their corresponding regions)
export AWS_IMAGES="ami-1f8c8e6b,ami-06875f1b,ami-0bb92e31,ami-e50e888c"
# Hours when a benchmark run should be triggered
export BENCH_HOURS="0,3,6,9,12,15,18,21"

# Install chef configuration management system from source.
curl -L https://www.opscode.com/chef/install.sh | sudo bash

# Install some basic packages
aptitude update
aptitude -y install git

# Clone necessary repositories for benchmarking.
mkdir /var/chef
mkdir /var/chef/cookbooks

cd /var/chef/cookbooks
git clone https://nanekratzke@bitbucket.org/nanekratzke/iaas-bench-controller.git

# Install the benchmark
chef-solo -l debug -L /root/chef-install.log -j /var/chef/cookbooks/iaas-bench-controller/install.json