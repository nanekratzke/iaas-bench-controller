# IaaS Benchmark Controller #

This chef cookbook is used to install a controller node which is triggering
benchmark runs on several nodes running in an AWS EC2 (IaaS) environment. This cookbook
is used for systematic benchmark surveys in the AWS EC2 infrastructure.

It is tested on Ubuntu 12.04 LTS operating systems. It may work on other operating systems,
but this is not tested.

This benchmarking suite on the benchmarking nodes is installed by another
[IaaS Benchmarking](http://bitbucket.org/nanekratzke/iaas-benchmarking) cookbook.
Please refer to this cookbook to get the details how the benchmark is running and
uploading its data.

The controller has the periodically task to

+ install benchmark suites on various nodes according to this [cookbook](http://bitbucket.org/nanekratzke/iaas-benchmarking)
+ and to start their benchmarks runs.

The benchmarking nodes are responsible for collecting and uploading their data to a
S3 destination specified by a controller node.

## How to install and configure? ##

By principle you can install the cookbook on any Linux based operating system with
internet access. Nevertheless the most convenient form of install might be the installation
on an IaaS node (AWS instance). The controller does not need much processing power, so
a cost efficient t1.micro instance should be sufficient for running the controller.

Therefore you should provide the following cloud-init.sh startup script with your preferred
instance launching method (by API, by AWS Management Console, by EC2 command line tools, etc.).

```
#!/bin/bash

# Provide EC2 API credentials
export EC2_ACCESS_KEY="<EC2 API ACCESS KEY>"
export EC2_SECRET="<EC2 API SECRET ACCESS KEY>"

# Provide S3 credentials and bucket for uploading benchmark results
export S3_ACCESS_KEY="<S3 API ACCESS KEY>"
export S3_SECRET="<S3 API ACCESS KEY>"
export S3_BUCKET="<S3 BUCKET Identifier>"

# Provide benchmark setup
# Instance types to benchmark (use API identifiers)
export AWS_INSTANCE_TYPES="t1.micro,m1.small,m1.xlarge,m2.2xlarge,c1.medium,c1.xlarge"

# Regions to benchmark (use Region identifiers, configure as you like)
export AWS_REGIONS="eu-west-1,sa-east-1,ap-southeast-2,us-east-1"

# Images to use for benchmark (must have the same order as their corresponding regions)
# configure as you like (images must exist, they are not created for you...)
export AWS_IMAGES="ami-1f8c8e6b,ami-06875f1b,ami-0bb92e31,ami-e50e888c"

# Hours when a benchmark run should be triggered (so this example would run benchmarks
# at 00:00h, 03:00h, 06:00h, ..., 21:00h UTC
export BENCH_HOURS="0,3,6,9,12,15,18,21"

# --- You should not change the rest until you know what you are doing ! ---

# Install chef configuration management system from source.
curl -L https://www.opscode.com/chef/install.sh | sudo bash

# Install some basic packages
aptitude update
aptitude -y install git

# Clone necessary repositories for benchmarking.
mkdir /var/chef
mkdir /var/chef/cookbooks
cd /var/chef/cookbooks
git clone https://nanekratzke@bitbucket.org/nanekratzke/iaas-bench-controller.git

# Install the benchmark
chef-solo /var/chef/cookbooks/iaas-bench-controller/install.json
```

As you see, you will have to provide the following configuration data by setting
some enviroment variables.

+ EC2 access credentials (for the controller to launch EC2 benchmarking instances)
+ S3 access credentials and bucket (for the benchmarking instances to upload their benchmarking results)
+ AWS instance types which should be benchmarked (use their API names)
+ AWS regions to benchmark in (use their API names)
+ AWS images to use for benchmarking (in the same order as their corresponding regions)
+ Hours when the benchmarks should start

All the rest is responsible for

+ installing configuration management system [chef](http://wiki.opscode.com/display/chef/Home) (we use only chef-solo)
+ installing git and clone this cookbook to the node
+ running the install recipe provided by the benchmarking cookbook [here](http://bitbucket.org/nanekratzke/iaas-benchmarking)

## How is it working? ##

This cookbook provides the following recipes

+ **install recipe** to install a controller script.

The controller script is triggered by cron as you have specified by providing the
*BENCH_HOURS* environment variable passed through your cloud-init-script.
On each cron trigger the controller starts several benchmarking instances according to specified
regions and instance types via EC2 API calls. The benchmarking instances run their
benchmarks and upload their results to specified S3 bucket via [s3cmd](http://s3tools.org/s3cmd).

## How long is the controller running? ##

Right now, the controller has no automatic stopping feature included. So you have to stop
the controller by hand. Be aware of your costs. Benchmarking can be very expensive if run
uncontrolled.