name             "iaas-bench-controller"
maintainer       "Nane Kratzke"
maintainer_email "nane.kratzke@fh-luebeck.de"
license          "GPL somewhat"
description      "IaaS Benchmarking Controller"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.1"
