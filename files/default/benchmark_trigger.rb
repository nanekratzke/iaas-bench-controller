require 'rubygems'
require 'logger'
require 'yaml'
require 'aws-sdk'

# Read the configuration file provided via command line
config_file = ARGV.first
config = YAML.load_file(config_file)

# Initialize the logger
logfile = config["log"]["file"]
file = File.new(logfile, "a+")
log = Logger.new(file)
log.level = Logger::INFO

# Get instance types, images and region to use for the benchmark
instance_types = config["benchmark"]["instance_types"].split(",")
regions = config["benchmark"]["regions"].split(",")
images = config["benchmark"]["images"].split(",")

# Get S3 access credentials
s3_bucket = config["s3"]["bucket"]
s3_akid = config["s3"]["akid"]
s3_secret = config["s3"]["secret"]

# Get EC2 access credentials
ec2_akid = config["ec2"]["akid"]
ec2_secret = config["ec2"]["secret"]

# Generates user data for instance instantion
def user_data(itype, region, akid, secret, bucket)
	ud  = "#!/bin/bash\n"
	ud += "export AWS_INSTANCE_TYPE=#{itype}\n"
	ud += "export AWS_REGION=#{region}\n"
	ud += "export S3_ACCESS_KEY=#{akid}\n"
	ud += "export S3_SECRET=#{secret}\n"
	ud += "export S3_BUCKET=#{bucket}\n"
	ud += "aptitude -y install git\n"
	ud += "curl -L https://www.opscode.com/chef/install.sh | sudo bash\n"
	ud += "mkdir /var/chef\n"
	ud += "mkdir /var/chef/cookbooks\n"
	ud += "cd /var/chef/cookbooks\n"
	ud += "git clone https://nanekratzke@bitbucket.org/nanekratzke/iaas-benchmarking.git\n"
	ud += "chef-solo -j /var/chef/cookbooks/iaas-benchmarking/install.json\n"
	ud += "chef-solo -j /var/chef/cookbooks/iaas-benchmarking/run.json\n"
	ud += "chef-solo -j /var/chef/cookbooks/iaas-benchmarking/upload.json\n"
	ud += "chef-solo -j /var/chef/cookbooks/iaas-benchmarking/shutdown.json\n"
	return ud
end

# loop through all (regions, images) and instance types
regions.zip(images).each do |region, ami|
  begin
    # terminate still running benchmarking nodes
    # just in case if something went wrong during last benchmark run
    ec2 = AWS::EC2.new(
      :access_key_id => ec2_akid,
      :secret_access_key => ec2_secret,
      :ec2_endpoint => "ec2.#{region}.amazonaws.com"
    )

    ec2.instances.tagged("benchmarking").each do |i|
      begin
        i.terminate
        log.info "Terminated instance #{i} in region #{region}"
      rescue Exception => e
        log.error "Exception #{e} occured while terminating instance #{i}"
      end
    end

    instance_types.each do |type|
      begin

        i = ec2.instances.create(
          :image_id => ami,
          :user_data => user_data(type, region, s3_akid, s3_secret, s3_bucket),
          :instance_type => type,
          :instance_initiated_shutdown_behavior => "terminate"
        )

        i.tag("benchmarking")

        log.info "Created instance of type #{type} in region #{region}"
        log.debug "Provided user data " + user_data(type, region, s3_akid, s3_secret, s3_bucket)

      rescue Exception => e
        log.error e
        log.error user_data(type, region, s3_akid, s3_secret, s3_bucket)
      end
    end
  rescue Exception => e
    log.error "Exception #{e} occured while connecting to endpoint ec2.#{region}.amazonaws.com"
  end
end