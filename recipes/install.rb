#
# Cookbook Name:: IaaS Bench Controller
# Recipe:: install
#
# Copyright 2013, Nane Kratzke
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Update package lists (necessary for some installs)
execute "update package list" do
	command "aptitude update"
	action :run
end

# Install necessary packages to run the controller script
package "libxslt-dev"
package "libxml2-dev"
package "rubygems"
gem_package "aws-sdk"

# Install benchmark configuration
template "/root/conf.yaml" do
	source "config.erb"
	mode 0755
	owner "root"
	group "root"
end

# Install the benchmark trigger script
cookbook_file "/root/benchmark_trigger.rb" do
  source "benchmark_trigger.rb"
  mode 0755
end

# Install the cron trigger
for h in node[:hours].split(",") do
	cron "trigger-p#{h}" do
  		hour h
  		minute "2"
  		command "ruby /root/benchmark_trigger.rb /root/conf.yaml"
	end
end